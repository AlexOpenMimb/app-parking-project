import api from "./endpoint-api.js";

const d = document,
  $tbody = d.getElementById("discount-table"),
  $fragmetn = d.createDocumentFragment(),
  $form = d.getElementById("form-modal"),
  $loaded = d.getElementById("container-loaded-discount");

async function getDiscount() {
  try {
    let res = await axios.get(api.DISCOUNT),
      json = await res.data.data,
      i = 1;

    json.forEach((el) => {
      const $template = d.getElementById("template-table"),
        $templateContent = $template.content;

      $templateContent.querySelector("#table-item").textContent = i++;
      $templateContent.querySelector("#table-name").textContent = el.name;
      $templateContent.querySelector("#table-minutes").textContent = el.minutes;
      $templateContent.querySelector("#table-discount").textContent =
        el.discount;
      $templateContent.querySelector("#table-status").textContent = el.status;

      el.status === "Desactivado"
        ? $templateContent
            .querySelector("#table-status")
            .style.setProperty("background-color", "rgb(248, 158, 158)")
        : $templateContent
            .querySelector("#table-status")
            .style.setProperty("background-color", "rgb(157, 245, 157)");

      $templateContent.querySelector("#table-button button").dataset.id = el.id;
      $templateContent.querySelector("#table-button button").textContent =
        el.status === "Desactivado" ? "Activar" : "Desactivar";

      let $clone = d.importNode($templateContent, true);
      $fragmetn.appendChild($clone);
    });

    $tbody.appendChild($fragmetn);
  } catch (err) {
    console.log(err);
  }
}
getDiscount();

function activeButton() {
  d.addEventListener("click", async (e) => {
    if (e.target.matches(".discount-btn")) {
      let id = e.target.dataset,
        states = e.path[2].children[4].textContent,
        res;
      $loaded.classList.toggle("container-loaded-discount--actived");
      try {
        res = await axios.post(`${api.ACTIVDISCOUNT}${id.id}`, {
          state: states === "Activo" ? "0" : "1",
        });
        location.reload();
      } catch (err) {
        console.log(err);
      }
    }
  });
}

activeButton();

async function createDiscount() {
  d.addEventListener("submit", async (e) => {
    e.preventDefault();
    if (e.target === $form) {
      try {
        let res = await axios.post(api.DISCOUNT, {
          discount: {
            name: e.target.name.value,
            minutes: e.target.minutes.value,
            discount: e.target.discount.value,
          },
        });
      } catch (err) {
        console.log(err);
      }
    }
    location.reload();
  });
}

createDiscount();
