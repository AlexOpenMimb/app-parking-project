import { showErrorApi } from "../helper/error-message.js";
import api from "./endpoint-api.js";

const d = document,
  $form = d.getElementById("report-parking-form"),
  $inputFrom = d.getElementById("input-from-date"),
  $inputTo = d.getElementById("input-to-date"),
  $parrafo = d.getElementById("text-error"),
  $span = d.getElementById("text-succes"),
  $td_data = d.querySelectorAll(".h-1");

getReport();

async function getReport() {
  d.addEventListener("submit", async (e) => {
    e.preventDefault();
    if (e.target === $form) {
      if ($inputFrom.value === "" || $inputTo.value === "") {
        $parrafo.textContent = "No has ingresado el rango de fecha a consultar";
        setTimeout(() => {
          $parrafo.textContent = "";
        }, 3000);
      } else {
        try {
          $span.textContent = "Cargando los Datos...";
          let res = await axios.post(api.REPORT_REPEAT, {
              desde: $inputFrom.value,
              hasta: $inputTo.value,
            }),
            json = await res.data;

          console.log(res);
          console.log(json);
          $span.textContent = "";
          renderReport(json);
          $parrafo.textContent = json.messages;

          setTimeout(() => {
            $parrafo.textContent = "";
          }, 3000);
        } catch (err) {
          showErrorApi(err, $parrafo);
        }
      }
    }
  });
}

console.log($td_data);
function renderReport(json) {
  $td_data[0].textContent = json.position;
  $td_data[1].textContent = json.repeat;
  $td_data[2].textContent = json.type;
}
