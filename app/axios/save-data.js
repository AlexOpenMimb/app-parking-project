import { showErrorApi } from "../helper/error-message.js";

import api from "./endpoint-api.js";

const d = document,
  $form = d.getElementById("form-register"),
  $formModal = d.getElementById("form-modal"),
  $inputPark = d.getElementById("park-vehicle"),
  $inputTVehiclepark = d.getElementById("park-type-vehicle"),
  $inputHidden = d.getElementById("input-hidden"),
  $select = d.getElementById("select-v-i"),
  $selectModal = d.getElementById("index-select"),
  $template = d.getElementById("template").content,
  $fragment = d.createDocumentFragment();

saveData();
async function saveData() {
  d.addEventListener("submit", async (e) => {
    e.preventDefault();
    if (e.target === $form) {
      if (e.target.names.value === "") {
        const $spanForm = d.getElementById("span-form");
        $spanForm.classList.toggle("d-block");

        setTimeout(() => {
          $spanForm.classList.toggle("d-block");
        }, 2000);
      } else {
        try {
          const $spanApi = d.getElementById("span-api");
          $spanApi.classList.toggle("d-block");

          let number = Number($select.value);

          let res = await axios.post(api.VEHICLE, {
              client: {
                name: e.target.names.value,
                last_name: e.target.lastname.value,
                document: e.target.id.value,
              },
              vehicle: {
                plate: e.target.plate.value,
                brand: e.target.brand.value,
                model: e.target.model.value,
                id_vehicle_type: number,
              },
            }),
            json = await res.data.data;

          $spanApi.classList.toggle("d-block");

          renderModal(json.brand, json.plate, json.type_vehicle, json.id);

          showPosition(json.type_vehicle);

          showWindowModal();

          console.log(res);
          console.log(json);
        } catch (err) {
          console.log(err);
          showErrorApi(err, $form);
        }
      }
    }
  });
}

function renderModal(brand, plate, type_vehicle, id) {
  $inputPark.value = `${brand}, ${plate}`;
  $inputTVehiclepark.value = `${type_vehicle}`;
  $inputHidden.value = `${id}`;
}

async function showPosition(type) {
  try {
    let positionArray = [];

    let res = await axios.get(api.PARK_VEHICLE),
      json = await res.data.data;

    console.log(json);

    json.forEach((el) => {
      if (el.type_vehicle === type) {
        positionArray.push(el.position);
      }
    });
    console.log(positionArray);

    renderSelect(positionArray, type, ".option-template");
  } catch (err) {
    console.log(err);
  }
} // Deternminar el número de posiciones disponibles por tipo de vehículo.

function renderSelect(position, type, option) {
  let parkAvalilable;

  if (type === "Motocicleta") {
    parkAvalilable = 21;
  } else {
    parkAvalilable = 11;
  }

  for (let i = 1; i < parkAvalilable; i++) {
    if (!position.includes(i)) {
      $template.querySelector(option).textContent = `${type[0]}-${i}`;

      $template.querySelector(option).value = i;
      let $clone = d.importNode($template, true);
      $fragment.appendChild($clone);
      $selectModal.appendChild($fragment);
    } // Renderizar las posiciones de parqueo disponibles

    if (position.length === 10 || position.length === 20) {
      $template.querySelector(
        option
      ).textContent = `No hay parqueadero disponible para este tipo de vehículo`;
      let $clone = d.importNode($template, true);
      $fragment.appendChild($clone);
      $selectModal.appendChild($fragment);
    } //Renderizar cuando no hay posiciones de parqueo disponibles
  }
}

function showWindowModal() {
  d.querySelector(".container-modal").classList.toggle(
    "container-modal--active"
  );
  d.querySelector(".container-modal__window").classList.toggle(
    "container-modal__window--active"
  );
}

savePark();

async function savePark() {
  d.addEventListener("submit", async (e) => {
    if (e.target === $formModal) {
      console.log(Number($inputHidden.value));
      console.log(Number($selectModal.value));

      try {
        const $spanMessage = d.getElementById("park-message");
        $spanMessage.textContent = "Paqueando vehículo...";

        let res = await axios.post(api.PARK_VEHICLE, {
            vehicle: {
              id: Number($inputHidden.value),
              position: Number($selectModal.value),
            },
          }),
          json = await res.data;

        $spanMessage.textContent = "Vehículo parquedo exitosamente";

        setTimeout(() => {
          location.reload();
        }, 2000);

        console.log(res);
        console.log(json);
      } catch (err) {
        console.log(err);
      }
    }
  });
}
