import api from "./endpoint-api.js";

const d = document,
  $tbodyV = d.getElementById("vehicle-tbody"),
  $templateV = d.getElementById("vehicle-type-template").content,
  $fragmentV = d.createDocumentFragment(),
  $tableV = d.getElementById("vehicle-table"),
  $loadedV = d.querySelector(".container-loaded-discount");

async function getVehicle() {
  try {
    let res = await axios.get(api.VEHICLE),
      json = await res.data.data,
      i = 1;

    json.forEach((el) => {
      $templateV.querySelector("#v-item-tamplate").textContent = i++;
      $templateV.querySelector("#v-brand-tamplate").textContent = el.brand;
      $templateV.querySelector("#v-plaque-tamplate").textContent = el.plate;
      $templateV.querySelector("#tv-typeVehicle-tamplate").textContent =
        el.type_vehicle;
      $templateV.querySelector("#tv-owner-tamplate").textContent = el.client;
      $templateV.querySelector("#tv-ownerid-tamplate").textContent =
        el.docmuent;
      $templateV.querySelector("#tv-date-tamplate").textContent = el.date;
      $templateV.querySelector("#tv-state-tamplate").textContent = el.status;

      let $clone = d.importNode($templateV, true);
      $fragmentV.appendChild($clone);
    });
    $loadedV.classList.toggle("container-loaded-discount--actived");

    $tbodyV.appendChild($fragmentV);

    console.log(res);
    console.log(json);
  } catch (err) {
    $loadedV.classList.toggle("container-loaded-discount--actived");
    console.log(err);
    let message = err.request.statusText || "Ocurrio un error";
    $tableV.insertAdjacentHTML(
      "afterend",
      `<p class="api-error-message">${message} : Error code ${err.request.status}</p>`
    );
  }
}

d.addEventListener("DOMContentLoaded", () => {
  $loadedV.classList.toggle("container-loaded-discount--actived");
  getVehicle();
});
