import api from "./endpoint-api.js";

const d = document,
  $tableCar = d.getElementById("car"),
  $tableCar_2 = d.getElementById("car-2"),
  $tableMotorBike = d.getElementById("motor-bike"),
  $tableMotorBike_2 = d.getElementById("motor-bike-2"),
  $tableBike = d.getElementById("bike"),
  $selectVehicle = d.getElementById("vehicle-list"),
  $selectPosition = d.getElementById("position-park-select"),
  $template = d.getElementById("template").content,
  $templateModal = d.getElementById("template-modal").content,
  $fragment = d.createDocumentFragment(),
  $form = d.getElementById("save-park");

let car = [],
  motor = [],
  bike = [],
  id_exit = "start";

parkingRegister();

async function parkingRegister() {
  let res = await axios.get(api.PARK_VEHICLE),
    json = await res.data.data;

  console.log(json);

  json.forEach((el) => {
    if (el.type_vehicle === "Automóvil") {
      car.push(el.position);
    }
    if (el.type_vehicle === "Motocicleta") {
      motor.push(el.position);
    }
    if (el.type_vehicle === "Bicicleta") {
      bike.push(el.position);
    }
  });

  renderTable("Automóvil", json, $tableCar, car, 6, 11);
  renderTable("Automóvil", json, $tableCar_2, car, 1, 6);
  renderTable("Bicicleta", json, $tableBike, bike, 1, 11);
  renderTable("Motocicleta", json, $tableMotorBike, motor, 1, 11);
  renderTable("Motocicleta", json, $tableMotorBike_2, motor, 11, 21);
}

function renderTable(type_v, json, table, array, it, position) {
  for (let i = it; i < position; i++) {
    if (array.includes(i)) {
      ocupatedPark(i, type_v, table, json);
    } else {
      $template.querySelector(
        ".vehicle_register__cells"
      ).textContent = `${type_v[0]}-${i}`;
      $template.querySelector(".vehicle_register__cells").style.background =
        "rgb(109, 109, 233)";
      $template.querySelector(".vehicle_register__cells").title = "";
      $template.querySelector(".vehicle_register__cells").dataset.park =
        "available";
      let $clone = d.importNode($template, true);
      $fragment.appendChild($clone);
      table.appendChild($fragment);
    }
  }
}

function ocupatedPark(it, type_v, table, json) {
  json.forEach((el) => {
    if (el.type_vehicle === type_v && el.position === it) {
      $template.querySelector(
        ".vehicle_register__cells"
      ).textContent = `${type_v[0]}-${el.position} Placa: ${el.plate}`;
      $template.querySelector(".vehicle_register__cells").style.background =
        "red";
      $template.querySelector(
        ".vehicle_register__cells"
      ).title = `${el.name}, ${el.document}`;
      $template.querySelector(".vehicle_register__cells").id = el.id;
      $template.querySelector(".vehicle_register__cells").dataset.park =
        "unavailable";
      let $clone = d.importNode($template, true);
      $fragment.appendChild($clone);
      table.appendChild($fragment);
    }
  });
}

renderVehicleRegister();

async function renderVehicleRegister() {
  let res = await axios.get(api.VEHICLE),
    json = await res.data.data;

  console.log(json);

  json.forEach((el) => {
    $templateModal.querySelector(
      "option"
    ).value = `${el.type_vehicle[0]}${el.id}`;

    $templateModal.querySelector(
      "option"
    ).textContent = `${el.type_vehicle} - Placa ${el.plate} - ${el.client}`;

    let $clone = d.importNode($templateModal, true);
    $fragment.appendChild($clone);
    $selectVehicle.appendChild($fragment);
  });
}

showPositioPark();

function showPositioPark() {
  let type;

  d.addEventListener("change", (e) => {
    if (e.target === $selectVehicle) {
      let vehicle = $selectVehicle.value[0];

      if (vehicle === "A") {
        type = "Automóvil";

        renderOption(type, car);
      } else if (vehicle === "M") {
        type = "Motocicleta";

        renderOption(type, motor);
      } else if (vehicle === "B") {
        type = "Bicicleta";

        renderOption(type, bike);
      }
    }
  });
}

/* renderSelect(car, "Automóvil", "option"); */

function renderOption(type, array_V) {
  let capacite;
  if (type === "Motocicleta") {
    capacite = 21;
  } else {
    capacite = 11;
  }
  let html = `<option  value aria-disabled>Selecionar</option>`;
  for (let i = 1; i < capacite; i++) {
    if (!array_V.includes(i)) {
      html += `<option value="${i}">${type[0]}-${i}</option>`;
      $selectPosition.innerHTML = html;
    }
  }
}

savePark();

async function savePark() {
  d.addEventListener("submit", async (e) => {
    e.preventDefault();
    if (e.target === $form) {
      let id_v = $selectVehicle.value.slice(1);
      const $span = d.getElementById("parking-message");
      try {
        $span.textContent = "Parqueando Vehículo ...";
        let res = await axios.post(api.PARK_VEHICLE, {
            vehicle: {
              id: id_v,
              position: $selectPosition.value,
            },
          }),
          json = await res.data;

        console.log(res);
        console.log(json.messages);
        $span.textContent = "Vehículo parqueado exitosamente";
        setTimeout(() => {
          location.reload();
        }, 1000);
      } catch (err) {
        console.log(err);
      }

      console.log($selectVehicle.value);
      console.log(id_v);
    }
  });
}

greenPark();

function greenPark() {
  d.addEventListener("click", (e) => {
    if (e.target.matches(".vehicle_register__cells")) {
      if (e.path[0].style.background === "green" && id_exit === id_exit) {
        console.log("Work!");

        const greenCell = d.getElementById(id_exit);

        e.path[0].style.background = "red";
      }
      if (e.path[0].dataset.park === "unavailable" && id_exit === "start") {
        e.path[0].style.background = "green";
        id_exit = e.path[0].id;
      }

      if (e.path[0].dataset.park === "unavailable" && id_exit === id_exit) {
        const newCell = d.getElementById(id_exit);
        newCell.style.background = "red";
        e.path[0].style.background = "green";
        id_exit = e.path[0].id;
      }
    }
    console.log(id_exit);
    console.log(e.path[0].style.background);
  });
}

parkingExit();

async function parkingExit() {
  d.addEventListener("click", async (e) => {
    if (e.target.matches("#exit-btn")) {
      const $messageError = d.getElementById("messase-process-error"),
        $messageParking = d.getElementById("messase-process-exit");

      if (id_exit === "start") {
        $messageError.textContent = "No ha seleccionado el vehículo";
        setTimeout(() => {
          $messageError.textContent = "";
        }, 2000);
      } else {
        if (e.target.matches("#exit-btn")) {
          try {
            $messageParking.textContent = "Procesando Salida del Vehículo...";
            let res = await axios(`${api.PARK_VEHICLE}/${id_exit}`, {
                method: "PUT",
                headers: {
                  "Content-type": "application/json; charset=utf-8",
                },
              }),
              json = await res.data.data;

            $messageParking.textContent = "";
            console.log(res);
            console.log(json);
            Swal.fire({
              title: "<i>Vehículo fuera del parqueadero</i>",
              text: "Resumen de factura",
              html: contentTexthtml(json),
              icon: "success",
              confirmButtonText: "Aceptar",
            }).then(() => {
              location.reload();
            });
          } catch (err) {
            console.log(err);
          }
        }
      }
    }
  });
}

function contentTexthtml(json) {
  return `
   
<ul>
    <li class="d-flex j-c-between">
        <span class="f-wigth-7">Cliente</span>
        <span class="">${json.name}</span>
    </li>
    <li class="d-flex j-c-between">
        <span class="f-wigth-7">Documento</span>
        <span class="">${json.document}</span>
    </li>
    <li class="d-flex j-c-between">
        <span class="f-wigth-7">Vehículo</span>
        <span class="">${json.model}</span>
    </li>
    <li class="d-flex j-c-between">
        <span class="f-wigth-7">Placa</span>
        <span class="">${json.plate}</span>
    </li>
    <li class="d-flex j-c-between">
        <span class="f-wigth-7">Tipo de Vehícul</span>
        <span class="">${json.type_vehicle}</span>
    </li>
    <li class="d-flex j-c-between">
        <span class="f-wigth-7">Posición</span>
        <span class="">${json.type_vehicle[0]}-${json.position}</span>
    </li>
    <li class="d-flex j-c-between">
        <span class="f-wigth-7">Total</span>
        <span class="">${json.amount}</span>
    </li>
    <li class="d-flex j-c-between">
        <span class="f-wigth-7">Permanencia</span>
        <span class="">${json.duration}</span>
    </li>
    <li class="d-flex j-c-between">
        <span class="f-wigth-7">Fecha de Entrada</span>
        <span class="">${json.admission}</span>
    </li>
    <li class="d-flex j-c-between">
        <span class="f-wigth-7">Fecha de Salida</span>
        <span class="">${json.exit}</span>
    </li>
 </ul>

  `;
}
