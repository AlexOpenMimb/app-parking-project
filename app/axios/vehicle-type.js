import api from "./endpoint-api.js";

const d = document,
  $template = d.getElementById("vehicle-type-template").content,
  $fragmentTv = d.createDocumentFragment(),
  $tbody = d.getElementById("vehicle-type-tbody"),
  $loaded = d.querySelector(".container-loaded-discount"),
  $tableTv = d.getElementById("type-vehicle-table");

async function getTypeVehicle() {
  try {
    let res = await axios.get(api.TYPE_VEHICLE),
      json = await res.data.data,
      i = 1;

    console.log(res);
    console.log(json);
    json.forEach((el) => {
      $template.querySelector("#tv-item-tamplate").textContent = i++;
      $template.querySelector("#tv-name-tamplate").textContent = el.name;
      $template.querySelector("#tv-rate-tamplate").textContent = `$${el.rate}`;
      $template.querySelector("#tv-state-tamplate").textContent =
        el.status === "1" ? "Activo" : "Desactivado";

      let $clone = d.importNode($template, true);
      $fragmentTv.appendChild($clone);
    });
    $tbody.appendChild($fragmentTv);
    $loaded.classList.toggle("container-loaded-discount--actived");
  } catch (err) {
    $loaded.classList.toggle("container-loaded-discount--actived");
    console.log(err);
    let message = err.request.statusText || "Ocurrio un error";
    $tableTv.insertAdjacentHTML(
      "afterend",
      `<p class="api-error-message">${message} : Error code ${err.request.status}</p>`
    );
  }
}

d.addEventListener("DOMContentLoaded", () => {
  $loaded.classList.toggle("container-loaded-discount--actived");
  getTypeVehicle();
});
