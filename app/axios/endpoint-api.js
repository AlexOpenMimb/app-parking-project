const DOMAIN = "http://127.0.0.1:8000/",
  DISCOUNT = `${DOMAIN}api/discount`,
  ACTIVDISCOUNT = `${DOMAIN}api/handle/discount/`,
  TYPE_VEHICLE = `${DOMAIN}api/type/vehicle`,
  VEHICLE = `${DOMAIN}api/vehicle`,
  REPORT_REPEAT = `${DOMAIN}api/report/repeat`,
  SALE_REPORT = `${DOMAIN}api/report/amount`,
  PARK_VEHICLE = `${DOMAIN}api/register`,
  REPORT_VEHICEL = `${DOMAIN}api/report/vehicle`;

export default {
  DOMAIN,
  DISCOUNT,
  ACTIVDISCOUNT,
  TYPE_VEHICLE,
  VEHICLE,
  REPORT_REPEAT,
  SALE_REPORT,
  PARK_VEHICLE,
  REPORT_VEHICEL,
};

/* "https://system-parking-bellpi.herokuapp.com/api/handle/discount/2"; */

/* https://127.0.0.1:8000/ */
