import { showErrorApi } from "../helper/error-message.js";
import api from "./endpoint-api.js";

const d = document,
  $formT = d.getElementById("transition-form"),
  $inputFrom = d.getElementById("transition-from"),
  $inputTo = d.getElementById("transition-to"),
  $span = d.getElementById("text-succes"),
  $parrafo = d.getElementById("transtion-message"),
  $template = d.getElementById("transition-template").content,
  $fragment = d.createDocumentFragment(),
  $sectionTable = d.getElementById("section-table"),
  $tbody = d.getElementById("transition-tbody");

async function getTransition() {
  d.addEventListener("submit", async (e) => {
    e.preventDefault();
    if (e.target === $formT) {
      if ($inputFrom.value === "" || $inputTo.value === "") {
        $parrafo.textContent = "No has ingresado el rango de fecha a consultar";
        setTimeout(() => {
          $parrafo.textContent = "";
        }, 3000);
      } else {
        try {
          $span.textContent = "Consultando Datos...";
          let res = await axios.post(api.REPORT_VEHICEL, {
              desde: $inputFrom.value,
              hasta: $inputTo.value,
            }),
            json = await res.data.data;
          $span.textContent = "";
          $sectionTable.classList.toggle("d-flex");
          let i = 1;
          json.forEach((el) => {
            $template.querySelector("#tran-item").textContent = i++;
            $template.querySelector("#tran-name").textContent = el.name;
            $template.querySelector("#tran-document").textContent = el.document;
            $template.querySelector("#tran-vehicle").textContent =
              el.type_vehicle;
            $template.querySelector("#tran-status").textContent = el.status;
            $template.querySelector("#tran-discount").textContent =
              el.discount_percentage === null
                ? "Sin Descuento"
                : `${el.discount_percentage}%`;
            $template.querySelector("#tran-rate").textContent = `$${el.rate}`;
            $template.querySelector(
              "#tran-total"
            ).textContent = `$${el.amount}`;

            let $clone = d.importNode($template, true);
            $fragment.appendChild($clone);
          });
          $tbody.appendChild($fragment);

          console.log(json);
        } catch (err) {
          showErrorApi(err, $parrafo);
        }
      }
    }
  });
}

getTransition();
