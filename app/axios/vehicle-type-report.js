import { showErrorApi } from "../helper/error-message.js";

import api from "./endpoint-api.js";

const d = document,
  $formRtv = d.getElementById("form-rtv"),
  $inputFrom = d.getElementById("input-from-rtv"),
  $inputTo = d.getElementById("input-to-rtv"),
  $parrafoVtr = d.getElementById("error-message"),
  $parraSucces = d.getElementById("text-succes"),
  $select = d.getElementById("select"),
  $tbody_2 = d.getElementById("report-tv-tbody");

getVehicle();

async function getVehicle() {
  d.addEventListener("submit", async (e) => {
    e.preventDefault();
    if (e.target === $formRtv) {
      if ($inputFrom.value === "" || $inputTo.value === "") {
        $parrafoVtr.textContent =
          "No has ingresado el rango de fecha a consultar";
        setTimeout(() => {
          $parrafoVtr.textContent = "";
        }, 2000);
        return;
      } else if ($select.value === "Seleccionar") {
        $parrafoVtr.textContent = "Seleccione un tipo de vehículo";
        setTimeout(() => {
          $parrafoVtr.textContent = "";
        }, 2000);
      } else {
        try {
          $parraSucces.textContent = "Consultando Datos...";
          let res = await axios.post(api.REPORT_VEHICEL, {
              desde: $inputFrom.value,
              hasta: $inputTo.value,
              tipo: $select.value,
            }),
            json = await res.data.data;

          $parraSucces.textContent = "";

          renderTable(json);

          console.log(json);
        } catch (err) {
          $parraSucces.textContent = "";
          showErrorApi(err, $parrafoVtr);
        }
      }
    }
  });
}

function renderTable(json) {
  let i = 1;
  let html = ` <tr class="d-none">
      <td id="report-tv-item"></td>
      <td id="report-tv-name"></td>
      <td id="report-tv-document"></td>                   
      <td id="report-tv-status"></td>
      <td id="report-tv-discount"></td>
      <td id="report-tv-rate"></td>
  </tr>`;

  json.forEach((el) => {
    let discount =
      el.discount_percentage === null
        ? `Sin Descuento`
        : `${el.discount_percentage}%`;
    html += `       
  <tr  class="template-table__tr">
      <td id="report-tv-item">${i++}</td>
      <td id="report-tv-name">${el.name}</td>
      <td id="report-tv-document">${el.document}</td>                   
      <td id="report-tv-status">${el.status}</td>
      <td id="report-tv-discount">${discount}</td>
      <td id="report-tv-rate">$${el.rate}</td>
  </tr>
    
    `;

    $tbody_2.innerHTML = html;
  });
}
