import { showErrorApi } from "../helper/error-message.js";
import api from "./endpoint-api.js";

const d = document,
  $formS = d.getElementById("sale-form"),
  $inputFromS = d.getElementById("sale-input-from"),
  $inputToS = d.getElementById("sale-input-to"),
  $parrafoS = d.getElementById("error-message-sale"),
  $span = d.getElementById("text-succes-sale"),
  $tbody = d.getElementById("report-sale-tbody");

async function getSaleReport() {
  d.addEventListener("submit", async (e) => {
    e.preventDefault();
    if (e.target === $formS) {
      if ($inputFromS.value === "" || $inputToS.value === "") {
        $parrafoS.textContent =
          "No has ingresado el rango de fecha a consultar";
        setTimeout(() => {
          $parrafoS.textContent = "";
        }, 3000);
      } else {
        $span.textContent = `Consultando Datos ...`;
        try {
          let res = await axios.post(api.SALE_REPORT, {
              desde: $inputFromS.value,
              hasta: $inputToS.value,
            }),
            json = await res.data.data;

          $span.textContent = "";

          saleReport(json);

          $parrafoS.textContent = json.messages;
          setTimeout(() => {
            $parrafoS.textContent = "";
          }, 3000);

          console.log(json);
        } catch (err) {
          showErrorApi(err, $parrafoS);
        }
      }
    }
  });
}

getSaleReport();

function saleReport(json) {
  let i = 1,
    html = `<tr  class="d-none">
      <td id="report-tv-item"></td>
      <td id="report-tv-name"></td>
      <td id="report-tv-document"><td>                   
    
  </tr>`;
  json.forEach((el) => {
    html += `       
  <tr  class="template-table__tr">
      <td id="report-tv-item">${i++}</td>
      <td id="report-tv-name">${el.day}</td>
      <td id="report-tv-document">$${el.amount}</td>                   
    
  </tr>
    
    `;

    $tbody.innerHTML = html;
  });
}
