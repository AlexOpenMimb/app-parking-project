import hamburguerMenu, { subMenu } from "./event/hamburger_menu.js";

const d = document;

d.addEventListener("DOMContentLoaded", (e) => {
  hamburguerMenu(".header-container__button", "#menu-nav");

  subMenu(".header-container__link");
});
