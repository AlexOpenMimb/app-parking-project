//This is the event programing for show and hide the modal window.
const d = document;

showModal("#modal-btn");

showModal("#edit-btn");

showModal(".show-modal-discount");

showModal(".vehicle_register__btn-get-parking");

closeModalWindonw(".container-modal");

closeModalWindonw(".close-modal");

closeModalWindonw(".container-modal__btn--grys");

function showModal(btnModal) {
  d.addEventListener("click", (e) => {
    if (e.target.matches(btnModal)) {
      d.querySelector(".container-modal").classList.toggle(
        "container-modal--active"
      );
      d.querySelector(".container-modal__window").classList.toggle(
        "container-modal__window--active"
      );
    }
  });
}

function closeModalWindonw(closeModal) {
  d.addEventListener("click", (e) => {
    if (e.target.matches(closeModal)) {
      d.querySelector(".container-modal__window").classList.toggle(
        "container-modal__window--active"
      );
      setTimeout(() => {
        d.querySelector(".container-modal").classList.toggle(
          "container-modal--active"
        );
      }, 1000);
    }
  });
}

/* */
/*  */
