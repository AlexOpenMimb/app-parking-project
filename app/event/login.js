const d = document,
  userData = {
    user: "alex@gmail.com",
    password: "12345",
  };
let formName = d.getElementById("login-input-name"),
  formPassword = d.getElementById("input-pasword");

function showPasw(eyeBtn) {
  d.addEventListener("click", (e) => {
    if (e.target.matches(eyeBtn)) {
      if (e.path[1].nextElementSibling.type === "password") {
        e.path[1].nextElementSibling.type = "text";
        e.path[0].classList.remove("fa-eye-slash");
        e.path[0].classList.add("fa-eye");
      } else {
        e.path[1].nextElementSibling.type = "password";
        e.path[0].classList.add("fa-eye-slash");
        e.path[0].classList.remove("fa-eye");
      }
    }
  });
}

showPasw("#btn-eye");

function validateUser(validate) {
  d.addEventListener("click", (e) => {
    if (e.target.matches(validate)) {
      if (
        userData.user == formName.value &&
        userData.password == formPassword.value
      ) {
        console.log(formName, formPassword);
        window.location = "./index.html";
      } else {
        console.log("Usuario invalido");
      }
    }
  });
}

validateUser("#btn-login");
