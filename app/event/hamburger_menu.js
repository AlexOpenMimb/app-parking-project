//This is the event programing for show and hide the menu hamburger.
const d = document;

export default function hamburguerMenu(buttonMenu, menu) {
  d.addEventListener("click", (e) => {
    if (e.target.matches(buttonMenu) || e.target.matches(`${buttonMenu} *`)) {
      d.querySelector(menu).classList.toggle(
        "header-container__navbar--active"
      );
    }
  });
  
}

export function subMenu(link) {
  d.addEventListener("click", (e) => {
    if (e.target.matches(link)) {
      e.preventDefault();
      d.querySelectorAll(link).forEach((el) => {
        if (window.innerWidth < 992) {
          e.target.nextElementSibling.classList.toggle(
            "header-container__submenu--active"
          );
        }
      });
    }
    if (e.target.matches(`${link} span`)) {
      e.preventDefault();
      if (window.innerWidth < 992) {
        e.path[1].nextElementSibling.classList.toggle(
          "header-container__submenu--active"
        );
      }
    }
  });
}
