const dc = document,
  $input = dc.querySelectorAll(".container-modal_input"),
  $subMitBtn = dc.getElementById("container-modal__btn"),
  $errorEmpty = dc.getElementById("error-submit"),
  $nameTitle = dc.getElementById("discount-name-title"),
  $discoTitle = dc.getElementById("discount-disc-title"),
  $minutesTitle = dc.getElementById("discount-minutes-title"),
  exrName = /^[A-Za-z0-9ÑñÁáÉéÍíÓóÚúÜü\s\-\_]{1,50}$/,
  exrNumber = /^\d+$/;

let errorInput = {
  name: false,
  discount: false,
  minutes: false,
};

function validateInput(expRegu, inputValue, input, target) {
  if (expRegu.test(inputValue)) {
    input.classList.remove("container-modal_input--error-name");
    $nameTitle.classList.remove("container-modal__instruction-tittle-error");

    errorInput[target] = true;
  } else {
    input.classList.add("container-modal_input--error-name");
    $nameTitle.classList.add("container-modal__instruction-tittle-error");
    errorInput[target] = false;
  }
}

function validateDiscount(expRegu, inputValue, input, target) {
  if (expRegu.test(inputValue) && inputValue <= 50) {
    input.classList.remove("container-modal_input--error-name");
    $discoTitle.classList.remove("container-modal__instruction-tittle-error");
    errorInput[target] = true;
  } else {
    input.classList.add("container-modal_input--error-name");
    $discoTitle.classList.add("container-modal__instruction-tittle-error");

    errorInput[target] = false;
  }
}
function validateMinute(expRegu, inputValue, input, target) {
  if (expRegu.test(inputValue) && inputValue <= 120) {
    input.classList.remove("container-modal_input--error-name");
    $minutesTitle.classList.remove("container-modal__instruction-tittle-error");

    errorInput[target] = true;
  } else {
    input.classList.add("container-modal_input--error-name");
    $minutesTitle.classList.add("container-modal__instruction-tittle-error");

    errorInput[target] = false;
  }
}

function validate(e) {
  switch (e.target.name) {
    case "name":
      validateInput(exrName, e.target.value, e.target, e.target.name);
      changeBtn();

      break;
    case "discount":
      validateDiscount(exrNumber, e.target.value, e.target, e.target.name);
      changeBtn();

      break;
    case "minutes":
      validateMinute(exrNumber, e.target.value, e.target, e.target.name);

      changeBtn();

      break;
  }
}

function formValidate() {
  $input.forEach((el) => {
    el.addEventListener("keyup", validate);
  });
}

function changeBtn() {
  if (errorInput.name && errorInput.discount && errorInput.minutes) {
    $subMitBtn.type = "submit";
  } else {
    $subMitBtn.type = "button";
  }
}

function showError(btn) {
  dc.addEventListener("click", (e) => {
    if (e.target.matches(btn)) {
      errorBorder();

      $errorEmpty.classList.add("container-modal__instruction--active");
      $errorEmpty.classList.remove("container-modal__instruction--none");
      setTimeout(() => {
        $errorEmpty.classList.remove("container-modal__instruction--active");
        $errorEmpty.classList.add("container-modal__instruction--none");
      }, 3000);
    }
  });
}

function errorBorder() {
  $input.forEach((inp) => {
    if (inp.value === "") {
      inp.classList.add("container-modal_input--error-name");
      setTimeout(() => {
        inp.classList.remove("container-modal_input--error-name");
      }, 3000);
    } else {
      inp.classList.remove("container-modal_input--error-name");
    }
  });
}

showError("#container-modal__btn");

formValidate();
