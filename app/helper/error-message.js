export function showErrorApi(err, parrafo) {
  let message = err.request.statusText || "Ocurrio un error";

  parrafo.insertAdjacentHTML(
    "afterend",

    `<span class="api-error-message"> Se ha presentado un error al conectarse con el servidor:</span>
          <span >${message} : Error code ${err.request.status}</span>`
  );
}
